import pytest

from romancalculator import __version__
from romancalculator.romancalculator import RomanCalculator


def test_version():
    assert __version__ == '0.1.0'


class TestRomanCalculator:
    class TestAddition:
        @pytest.mark.parametrize('operands,expected', [
            (['I', 'I'], 'II'),
            (['I', 'I', "I"], 'III'),
            (['II', 'II'], 'IV'),
            (['III', 'I'], 'IV'),
            (['IV', 'I'], 'V'),
            (['III', 'II'], 'V'),
            (['V', 'V'], 'X'),
            (['VI', 'IIII'], 'X'),
            (['X', 'VI'], 'XVI'),
        ])
        def test_nominal_case(self, operands, expected):
            # arrange
            # action
            result: str = RomanCalculator().addition(*operands)
            # assert
            assert expected == result
