class RomanCalculator:
    def addition(self, *operands):
        roman_table: dict = {
            "IV": "IIII",
            "V": "IIIII",
            "VI": "IIIIII",
            "X": "IIIIIIIIII"
        }
        inverted_roman_table: dict = {value: key for key, value in roman_table.items()}

        converted_to_I: str = "".join([roman_table.get(operand, operand) for operand in operands])

        return inverted_roman_table.get(converted_to_I,  converted_to_I)
